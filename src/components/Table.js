import React, { Component } from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import axios from 'axios';

export default class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
          allAdresses: [],
          adresses: []
        };
    }

    componentWillMount() {
        this.getAdresses();
    };

    filterList = event => {
        var list = this.state.allAdresses;
        list = list.filter(item => {
          return (
            item.fields.tco_libelle
                .toLowerCase()
                .indexOf(event.target.value.toLowerCase()) !== -1 ||
            item.fields.ville
                .toLowerCase()
                .indexOf(event.target.value.toLowerCase()) !== -1 ||
            item.fields.code_postal
                .toString()
                .toLowerCase()
                .indexOf(event.target.value.toLowerCase()) !== -1
          );
        });
        this.setState({
          adresses: list
        });
    };
    
    getAdresses = () => {
        let url = `https://data.ratp.fr/api/records/1.0/search/?dataset=liste-des-commerces-de-proximite-agrees-ratp&lang=fr&rows=100&sort=code_postal&facet=tco_libelle&facet=code_postal&facet=ville&geofilter.distance=1000`;
        axios.get(url).then((response) => {
           console.log(response.data.records)
           let data = response.data.records;
           this.setState({
               allAdresses: data,
               adresses: data
            })
        }).catch((error) => {
          
        });

    };

    render() {
        const optionsPagination = {
            page: 1,
            paginationSize: 4,
            pageStartIndex: 1,
            hideSizePerPage: true,
            hidePageListOnlyOnePage: true
        };
      
        const columns = [
            {
              dataField: 'fields.tco_libelle',
              text: 'Libellé',
              sort: true,
              headerStyle: { fontSize: 10 }
            },
            {
                dataField: 'fields.ville',
                text: 'Ville',
                sort: true,
                headerStyle: { fontSize: 10 }
            },
            {
                dataField: 'fields.code_postal',
                text: 'Code postal',
                sort: true,
                headerStyle: { fontSize: 10 }
            }
          ];
        return (
        <div>
            <input
            onChange={this.filterList}
            className="form-control"
            type="text"
            placeholder="Rechercher"
            />
            <BootstrapTable
            keyField="recordid"
            data={this.state.adresses}
            columns={columns}
            bordered={false}
            pagination={paginationFactory(optionsPagination)}
            />
        </div>
        )
    }
}


